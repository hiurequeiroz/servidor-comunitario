#!/bin/bash

sudo apt install libnss3-tools
wget -c https://github.com/FiloSottile/mkcert/releases/download/v1.4.3/mkcert-v1.4.3-linux-arm64
chmod +x mkcert-v1.4.3-linux-arm64
./mkcert-v1.4.3-linux-arm64 -install
./mkcert-v1.4.3-linux-arm64 moinho.com
./mkcert-v1.4.3-linux-arm64 "*.moinho.com"
